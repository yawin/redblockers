import mods.exnihilo.Crucible;
import mods.exnihilo.Hammer;

//	_____ Crucible de Ex Nihilo (fuentes de calor) _____

			mods.exnihilo.Crucible.addHeatSource(<minecraft:coal_block>,		0.1);
			mods.exnihilo.Crucible.addHeatSource(<chisel:lavastone>,			0.2); // Calentar con piedras de lava de Chisel (1 x lava)
			mods.exnihilo.Crucible.addHeatSource(<chisel:lavastone:1>,			0.2); // Calentar con piedras de lava de Chisel (1 x lava)
			mods.exnihilo.Crucible.addHeatSource(<chisel:lavastone:2>,			0.2); // Calentar con piedras de lava de Chisel (1 x lava)
			mods.exnihilo.Crucible.addHeatSource(<chisel:lavastone:3>,			0.2); // Calentar con piedras de lava de Chisel (1 x lava)
			mods.exnihilo.Crucible.addHeatSource(<chisel:lavastone:4>,			0.2); // Calentar con piedras de lava de Chisel (1 x lava)
			mods.exnihilo.Crucible.addHeatSource(<chisel:lavastone:5>,			0.2); // Calentar con piedras de lava de Chisel (1 x lava)
			mods.exnihilo.Crucible.addHeatSource(<chisel:lavastone:6>,			0.2); // Calentar con piedras de lava de Chisel (1 x lava)
			mods.exnihilo.Crucible.addHeatSource(<chisel:lavastone:7>,			0.2); // Calentar con piedras de lava de Chisel (1 x lava)
			mods.exnihilo.Crucible.addHeatSource(<Botania:blazeBlock>,			0.3); // Calentar con Lampara de blaze de Botania (1.5 x lava)
			mods.exnihilo.Crucible.addHeatSource(<BigReactors:BRMetalBlock>,	0.4); // Calentar con bloque de Yellowrium (2 x lava)
			mods.exnihilo.Crucible.addHeatSource(<BigReactors:BRMetalBlock:3>,  0.8); // Calentar con bloque de Yellowrium (4 x lava)
			
//	_____ Crucible de Ex Nihilo (materiales nuevos) _____

			mods.exnihilo.Crucible.addRecipe( <minecraft:packed_ice>,	<liquid:water> * 2000	); // Fundir hielo compacto en agua (2 cubos)
			mods.exnihilo.Crucible.addRecipe( <chisel:marble>,			<liquid:lava> * 500		); // Fundir marmol en lava (0,5 cubos)
			mods.exnihilo.Crucible.addRecipe( <chisel:granite>,			<liquid:lava> * 1000	); // Fundir granito en lava (1 cubo)
			mods.exnihilo.Crucible.addRecipe( <chisel:diorite>,			<liquid:lava> * 800		); // Fundir diorita en lava (0,8 cubos)
			mods.exnihilo.Crucible.addRecipe( <chisel:andesite>,		<liquid:lava> * 800		); // Fundir andesita en lava (0,8 cubos)

//	-Receta 1 bloque de tritanio a 9 lingotes de tritanio

			recipes.addShapeless(<mo:tritanium_ingot> * 9, [<ore:blockTritanium>]);

	
