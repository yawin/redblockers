---- Minecraft Crash Report ----
// Shall we play a game?

Time: 11/11/15 6:59 PM
Description: Unexpected error

java.lang.ClassCastException: net.minecraft.client.multiplayer.WorldClient cannot be cast to net.minecraft.world.WorldServer
	at com.xcompwiz.mystcraft.world.profiling.WorldProviderMystDummy.replaceChunkProvider(WorldProviderMystDummy.java:206)
	at com.xcompwiz.mystcraft.world.profiling.WorldProviderMystDummy.calculateInitialWeather(WorldProviderMystDummy.java:200)
	at net.minecraft.world.World.func_72947_a(World.java:2674)
	at net.minecraft.world.World.finishSetup(World.java:207)
	at net.minecraft.client.multiplayer.WorldClient.<init>(WorldClient.java:59)
	at net.minecraft.client.network.NetHandlerPlayClient.func_147280_a(NetHandlerPlayClient.java:866)
	at net.minecraft.network.play.server.S07PacketRespawn.func_148833_a(SourceFile:32)
	at net.minecraft.network.play.server.S07PacketRespawn.func_148833_a(SourceFile:13)
	at net.minecraft.network.NetworkManager.func_74428_b(NetworkManager.java:212)
	at net.minecraft.client.multiplayer.PlayerControllerMP.func_78765_e(PlayerControllerMP.java:273)
	at net.minecraft.client.Minecraft.func_71407_l(Minecraft.java:1602)
	at net.minecraft.client.Minecraft.func_71411_J(Minecraft.java:973)
	at net.minecraft.client.Minecraft.func_99999_d(Minecraft.java:898)
	at net.minecraft.client.main.Main.main(SourceFile:148)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:497)
	at net.minecraft.launchwrapper.Launch.launch(Launch.java:135)
	at net.minecraft.launchwrapper.Launch.main(Launch.java:28)


A detailed walkthrough of the error, its code path and all known details is as follows:
---------------------------------------------------------------------------------------

-- Head --
Stacktrace:
	at com.xcompwiz.mystcraft.world.profiling.WorldProviderMystDummy.replaceChunkProvider(WorldProviderMystDummy.java:206)
	at com.xcompwiz.mystcraft.world.profiling.WorldProviderMystDummy.calculateInitialWeather(WorldProviderMystDummy.java:200)
	at net.minecraft.world.World.func_72947_a(World.java:2674)
	at net.minecraft.world.World.finishSetup(World.java:207)
	at net.minecraft.client.multiplayer.WorldClient.<init>(WorldClient.java:59)
	at net.minecraft.client.network.NetHandlerPlayClient.func_147280_a(NetHandlerPlayClient.java:866)
	at net.minecraft.network.play.server.S07PacketRespawn.func_148833_a(SourceFile:32)
	at net.minecraft.network.play.server.S07PacketRespawn.func_148833_a(SourceFile:13)
	at net.minecraft.network.NetworkManager.func_74428_b(NetworkManager.java:212)
	at net.minecraft.client.multiplayer.PlayerControllerMP.func_78765_e(PlayerControllerMP.java:273)

-- Affected level --
Details:
	Level name: MpServer
	All players: 1 total; [GCEntityClientPlayerMP['yawin'/3191, l='MpServer', x=-1.48, y=69.62, z=0.29]]
	Chunk stats: MultiplayerChunkCache: 325, 325
	Level seed: 0
	Level generator: ID 05 - BIOMESOP, ver 0. Features enabled: false
	Level generator options: 
	Level spawn location: World: (8,64,8), Chunk: (at 8,4,8 in 0,0; contains blocks 0,0,0 to 15,255,15), Region: (0,0; contains chunks 0,0 to 31,31, blocks 0,0,0 to 511,255,511)
	Level time: 108920 game time, 17996 day time
	Level dimension: 0
	Level storage version: 0x00000 - Unknown?
	Level weather: Rain time: 0 (now: false), thunder time: 0 (now: false)
	Level game mode: Game mode: creative (ID 1). Hardcore: false. Cheats: false
	Forced entities: 3 total; [EntityHat['desconocido'/431388, l='MpServer', x=-1.48, y=69.62, z=0.29], EntityTrail['desconocido'/431389, l='MpServer', x=-1.48, y=69.62, z=0.29], GCEntityClientPlayerMP['yawin'/3191, l='MpServer', x=-1.48, y=69.62, z=0.29]]
	Retry entities: 0 total; []
	Server brand: fml,forge
	Server type: Integrated singleplayer server
Stacktrace:
	at net.minecraft.client.multiplayer.WorldClient.func_72914_a(WorldClient.java:373)
	at net.minecraft.client.Minecraft.func_71396_d(Minecraft.java:2444)
	at net.minecraft.client.Minecraft.func_99999_d(Minecraft.java:927)
	at net.minecraft.client.main.Main.main(SourceFile:148)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:497)
	at net.minecraft.launchwrapper.Launch.launch(Launch.java:135)
	at net.minecraft.launchwrapper.Launch.main(Launch.java:28)

-- System Details --
Details:
	Minecraft Version: 1.7.10
	Operating System: Linux (amd64) version 3.16.0-51-generic
	Java Version: 1.8.0_66, Oracle Corporation
	Java VM Version: Java HotSpot(TM) 64-Bit Server VM (mixed mode), Oracle Corporation
	Memory: 2556530832 bytes (2438 MB) / 4559171584 bytes (4347 MB) up to 10724048896 bytes (10227 MB)
	JVM Flags: 5 total; -Xmx10G -XX:+UseConcMarkSweepGC -XX:+CMSIncrementalMode -XX:-UseAdaptiveSizePolicy -Xmn128M
	AABB Pool Size: 0 (0 bytes; 0 MB) allocated, 0 (0 bytes; 0 MB) used
	IntCache: cache: 14, tcache: 95, allocated: 0, tallocated: 0
	FML: MCP v9.05 FML v7.10.99.99 Minecraft Forge 10.13.4.1448 190 mods loaded, 190 mods active
	States: 'U' = Unloaded 'L' = Loaded 'C' = Constructed 'H' = Pre-initialized 'I' = Initialized 'J' = Post-initialized 'A' = Available 'D' = Disabled 'E' = Errored
	UCHIJAAAA	mcp{9.05} [Minecraft Coder Pack] (minecraft.jar) 
	UCHIJAAAA	FML{7.10.99.99} [Forge Mod Loader] (forge-1.7.10-10.13.4.1448-1.7.10.jar) 
	UCHIJAAAA	Forge{10.13.4.1448} [Minecraft Forge] (forge-1.7.10-10.13.4.1448-1.7.10.jar) 
	UCHIJAAAA	appliedenergistics2-core{rv2-stable-10} [AppliedEnergistics2 Core] (minecraft.jar) 
	UCHIJAAAA	Aroma1997Core{1.0.2.16} [Aroma1997Core] (Aroma1997Core-1.7.10-1.0.2.16.jar) 
	UCHIJAAAA	CodeChickenCore{1.0.6.43} [CodeChicken Core] (minecraft.jar) 
	UCHIJAAAA	ExtraFixes{1.2.4b} [Extra Fixes] (minecraft.jar) 
	UCHIJAAAA	InfiniBows{1.3.0 build 20} [InfiniBows] (minecraft.jar) 
	UCHIJAAAA	Micdoodlecore{} [Micdoodle8 Core] (minecraft.jar) 
	UCHIJAAAA	MobiusCore{1.2.5} [MobiusCore] (minecraft.jar) 
	UCHIJAAAA	NotEnoughItems{1.0.4.107} [Not Enough Items] (NotEnoughItems-1.7.10-1.0.4.107-universal.jar) 
	UCHIJAAAA	OpenModsCore{0.8} [OpenModsCore] (minecraft.jar) 
	UCHIJAAAA	<CoFH ASM>{000} [CoFH ASM] (minecraft.jar) 
	UCHIJAAAA	debug{1.0} [debug] (denseores-1.6.2.jar) 
	UCHIJAAAA	AncientWarfare{2.4.112-beta-MC1.7.10} [Ancient Warfare Core] (ancientwarfare-2.4.112-beta-MC1.7.10-FULL.jar) 
	UCHIJAAAA	CoFHCore{1.7.10R3.0.3} [CoFH Core] (CoFHCore-[1.7.10]3.0.3-303.jar) 
	UCHIJAAAA	BuildCraft|Core{7.0.21} [BuildCraft] (buildcraft-7.0.21.jar) 
	UCHIJAAAA	AncientWarfareAutomation{2.4.112-beta-MC1.7.10} [Ancient Warfare Automation] (ancientwarfare-2.4.112-beta-MC1.7.10-FULL.jar) 
	UCHIJAAAA	AncientWarfareNEIPlugin{2.4.112-beta-MC1.7.10} [Ancient Warfare NEI Plugin] (ancientwarfare-2.4.112-beta-MC1.7.10-FULL.jar) 
	UCHIJAAAA	AncientWarfareNpc{2.4.112-beta-MC1.7.10} [Ancient Warfare NPCs] (ancientwarfare-2.4.112-beta-MC1.7.10-FULL.jar) 
	UCHIJAAAA	AncientWarfareStructure{2.4.112-beta-MC1.7.10} [Ancient Warfare Structures] (ancientwarfare-2.4.112-beta-MC1.7.10-FULL.jar) 
	UCHIJAAAA	appliedenergistics2{rv2-stable-10} [Applied Energistics 2] (appliedenergistics2-rv2-stable-10.jar) 
	UCHIJAAAA	Aroma1997CoreHelper{1.0.2.16} [Aroma1997Core|Helper] (Aroma1997Core-1.7.10-1.0.2.16.jar) 
	UCHIJAAAA	AromaBackup{0.0.0.5} [AromaBackup] (AromaBackup-1.7.10-0.0.0.5.jar) 
	UCHIJAAAA	Baubles{1.0.1.10} [Baubles] (Baubles-1.7.10-1.0.1.10.jar) 
	UCHIJAAAA	aura{unspecified} [Aura Cascade] (AuraCascade-552.jar) 
	UCHIJAAAA	bdlib{1.9.2.104} [BD Lib] (bdlib-1.9.2.104-mc1.7.10.jar) 
	UCHIJAAAA	BiblioCraft{1.11.2} [BiblioCraft] (BiblioCraft[v1.11.2][MC1.7.10].jar) 
	UCHIJAAAA	Mantle{1.7.10-0.3.2.jenkins188} [Mantle] (Mantle-1.7.10-0.3.2a.jar) 
	UCHIJAAAA	Natura{2.2.0} [Natura] (natura-1.7.10-2.2.0.1.jar) 
	UCHIJAAAA	BiomesOPlenty{2.1.0} [Biomes O' Plenty] (BiomesOPlenty-1.7.10-2.1.0.1067-universal.jar) 
	UCHIJAAAA	BiblioWoodsBoP{1.9} [BiblioWoods Biomes O'Plenty Edition] (BiblioWoods[BiomesOPlenty][v1.9].jar) 
	UCHIJAAAA	Forestry{3.6.6.24} [Forestry for Minecraft] (forestry_1.7.10-3.6.6.24.jar) 
	UCHIJAAAA	BiblioWoodsForestry{1.7} [BiblioWoods Forestry Edition] (BiblioWoods[Forestry][v1.7].jar) 
	UCHIJAAAA	BiblioWoodsNatura{1.5} [BiblioWoods Natura Edition] (BiblioWoods[Natura][v1.5].jar) 
	UCHIJAAAA	ThermalFoundation{1.7.10R1.2.0} [Thermal Foundation] (ThermalFoundation-[1.7.10]1.2.0-102.jar) 
	UCHIJAAAA	ThermalExpansion{1.7.10R4.0.3B1} [Thermal Expansion] (ThermalExpansion-[1.7.10]4.0.3B1-218.jar) 
	UCHIJAAAA	BigReactors{0.4.3A} [Big Reactors] (BigReactors-0.4.3A.jar) 
	UCHIJAAAA	BinnieCore{2.0-pre14} [Binnie Core] (binnie-mods-2.0-pre14.jar) 
	UCHIJAAAA	Botany{2.0-pre14} [Botany] (binnie-mods-2.0-pre14.jar) 
	UCHIJAAAA	ExtraBees{2.0-pre14} [Extra Bees] (binnie-mods-2.0-pre14.jar) 
	UCHIJAAAA	ExtraTrees{2.0-pre14} [Extra Trees] (binnie-mods-2.0-pre14.jar) 
	UCHIJAAAA	Genetics{2.0-pre14} [Genetics] (binnie-mods-2.0-pre14.jar) 
	UCHIJAAAA	binocular{1.1} [Binocular] (Binocular-1.7.10-1.1.jar) 
	UCHIJAAAA	AWWayofTime{v1.3.3} [Blood Magic: Alchemical Wizardry] (BloodMagic-1.7.10-1.3.3-13.jar) 
	UCHIJAAAA	Thaumcraft{4.2.3.5} [Thaumcraft] (Thaumcraft-1.7.10-4.2.3.5.jar) 
	UCHIJAAAA	Botania{r1.7-221} [Botania] (Botania r1.7-221.jar) 
	UCHIJAAAA	BrandonsCore{1.0.0.6} [Brandon's Core] (BrandonsCore-1.0.0.6.jar) 
	UCHIJAAAA	BuildCraft|Silicon{7.0.21} [BC Silicon] (buildcraft-7.0.21.jar) 
	UCHIJAAAA	BuildCraft|Factory{7.0.21} [BC Factory] (buildcraft-7.0.21.jar) 
	UCHIJAAAA	BuildCraft|Builders{7.0.21} [BC Builders] (buildcraft-7.0.21.jar) 
	UCHIJAAAA	BuildCraft|Energy{7.0.21} [BC Energy] (buildcraft-7.0.21.jar) 
	UCHIJAAAA	BuildCraft|Robotics{7.0.21} [BC Robotics] (buildcraft-7.0.21.jar) 
	UCHIJAAAA	BuildCraft|Transport{7.0.21} [BC Transport] (buildcraft-7.0.21.jar) 
	UCHIJAAAA	CarpentersBlocks{3.3.7} [Carpenter's Blocks] (Carpenter's Blocks v3.3.7 - MC 1.7.10.jar) 
	UCHIJAAAA	catwalkmod{1.1.0} [Catwalks] (catwalkmod-1.7.10-1.1.0.jar) 
	UCHIJAAAA	ChickenChunks{1.3.4.19} [ChickenChunks] (ChickenChunks-1.7.10-1.3.4.19-universal.jar) 
	UCHIJAAAA	Railcraft{9.7.0.0} [Railcraft] (Railcraft_1.7.10-9.7.0.0.jar) 
	UCHIJAAAA	TwilightForest{2.3.7} [The Twilight Forest] (twilightforest-1.7.10-2.3.7.jar) 
	UCHIJAAAA	ForgeMultipart{1.2.0.345} [Forge Multipart] (ForgeMultipart-1.7.10-1.2.0.345-universal.jar) 
	UCHIJAAAA	chisel{2.5.1.44} [Chisel 2] (Chisel2-2.5.1.44.jar) 
	UCHIJAAAA	ComputerCraft{1.74} [ComputerCraft] (ComputerCraft1.74.jar) 
	UCHIJAAAA	cookingbook{1.0.92} [Cooking for Blockheads] (cookingbook-mc1.7.10-1.0.92.jar) 
	UCHIJAAAA	CustomMainMenu{1.7.1} [Custom Main Menu] (CustomMainMenu-MC1.7.10-1.7.1.jar) 
	UCHIJAAAA	darkcore{0.3} [Dark Core] (darkcore-0.3-45.jar) 
	UCHIJAAAA	DescriptionTags{1.0} [Description Tags] (DescriptionTags-1.0-1.7.10.jar) 
	UCHIJAAAA	DraconicEvolution{1.0.2-snapshot_3} [Draconic Evolution] (Draconic-Evolution-1.7.10-1.0.2-Snapshot_3.jar) 
	UCHIJAAAA	MineFactoryReloaded{1.7.10R2.8.0} [MineFactory Reloaded] (MineFactoryReloaded-[1.7.10]2.8.0-104.jar) 
	UCHIJAAAA	EnderIO{1.7.10-2.2.8.381} [Ender IO] (EnderIO-1.7.10-2.2.8.381.jar) 
	UCHIJAAAA	EnderStorage{1.4.7.33} [EnderStorage] (EnderStorage-1.7.10-1.4.7.33-universal.jar) 
	UCHIJAAAA	EnderTech{1.7.10-0.3.2.399} [EnderTech] (EnderTech-1.7.10-0.3.2.399.jar) 
	UCHIJAAAA	EnderZoo{1.7.10-1.0.15.32} [Ender Zoo] (EnderZoo-1.7.10-1.0.15.32.jar) 
	UCHIJAAAA	etfuturum{1.4.3} [Et Futurum] (Et Futurum-1.4.3.jar) 
	UCHIJAAAA	exnihilo{1.38-46} [Ex Nihilo] (Ex-Nihilo-1.38-46.jar) 
	UCHIJAAAA	RedstoneArsenal{1.7.10R1.1.1} [Redstone Arsenal] (RedstoneArsenal-[1.7.10]1.1.1-89.jar) 
	UCHIJAAAA	ExtraUtilities{1.2.11} [Extra Utilities] (extrautilities-1.2.11.jar) 
	UCHIJAAAA	ImmersiveEngineering{0.5.4} [Immersive Engineering] (ImmersiveEngineering-0.5.4.jar) 
	UCHIJAAAA	Waila{1.5.10} [Waila] (Waila-1.5.10_1.7.10.jar) 
	UCHIJAAAA	TConstruct{1.7.10-1.8.7.build979} [Tinkers' Construct] (TConstruct-1.7.10-1.8.7.jar) 
	UCHIJAAAA	exastris{MC1.7.10-1.16-36} [Ex Astris] (Ex-Astris-MC1.7.10-1.16-36.jar) 
	UCHIJAAAA	extracells{2.2.73} [Extra Cells 2] (ExtraCells-1.7.10-2.2.73b129.jar) 
	UCHIJAAAA	harvestcraft{1.7.10j} [Pam's HarvestCraft] (Pam's HarvestCraft 1.7.10j.jar) 
	UCHIJAAAA	progressiveautomation{1.6.27} [Progressive Automation] (ProgressiveAutomation-1.7.10-1.6.27.jar) 
	UCHIJAAAA	ExtraTiC{1.4.5} [ExtraTiC] (ExtraTiC-1.7.10-1.4.5.jar) 
	UCHIJAAAA	FastCraft{1.21} [FastCraft] (fastcraft-1.21.jar) 
	UCHIJAAAA	FlatSigns{2.1.0.19} [Flat Signs] (FlatSigns-1.7.10-universal-2.1.0.19.jar) 
	UCHIJAAAA	funkylocomotion{1.0} [Funky Locomotion] (funky-locomotion-1.7.10-beta-7.jar) 
	UCHIJAAAA	GalacticraftCore{3.0.12} [Galacticraft Core] (GalacticraftCore-1.7-3.0.12.357.jar) 
	UCHIJAAAA	GalacticraftMars{3.0.12} [Galacticraft Planets] (Galacticraft-Planets-1.7-3.0.12.357.jar) 
	UCHIJAAAA	GardenOfGlass{sqrt(-1)} [Garden of Glass] (GardenOfGlass.jar) 
	UCHIJAAAA	GardenContainers{1.7.10-1.6.8} [Garden Containers] (GardenStuff-1.7.10-1.6.8.jar) 
	UCHIJAAAA	GardenCore{1.7.10-1.6.8} [Garden Core] (GardenStuff-1.7.10-1.6.8.jar) 
	UCHIJAAAA	GardenStuff{1.7.10-1.6.8} [Garden Stuff] (GardenStuff-1.7.10-1.6.8.jar) 
	UCHIJAAAA	GardenTrees{1.7.10-1.6.8} [Garden Trees] (GardenStuff-1.7.10-1.6.8.jar) 
	UCHIJAAAA	MagicBees{1.7.10-2.3.5} [Magic Bees] (magicbees-1.7.10-2.3.5.jar) 
	UCHIJAAAA	gendustry{1.6.0.125} [GenDustry] (gendustry-1.6.0.125-mc1.7.10.jar) 
	UCHIJAAAA	guideapi{1.7.10-1.0.1-20} [Guide-API] (Guide-API-1.7.10-1.0.1-20.jar) 
	UCHIJAAAA	iChunUtil{4.2.2} [iChunUtil] (iChunUtil-4.2.2.jar) 
	UCHIJAAAA	Hats{4.0.1} [Hats] (Hats-4.0.1.jar) 
	UCHIJAAAA	HatStand{4.0.0} [HatStand] (HatStand-4.0.0.jar) 
	UCHIJAAAA	HelpFixer{1.0.7} [HelpFixer] (HelpFixer-1.0.7.jar) 
	UCHIJAAAA	immersiveintegration{0.5.4} [Immersive Integration] (immersiveintegration-0.5.4.jar) 
	UCHIJAAAA	inpure|core{1.7.10R1.0.0B9} [INpureCore] (INpureCore-[1.7.10]1.0.0B9-62.jar) 
	UCHIJAAAA	intellie{2.1.4.554} [IntelliE] (IntelligentEnergistics-1.7.10-2.1.4.554-universal.jar) 
	UCHIJAAAA	appaero{2.1.4.554} [AppliedAerodynamics] (IntelligentEnergistics-1.7.10-2.1.4.554-universal.jar) 
	UCHIJAAAA	appaeronei{2.1.4.554} [AppliedAerodynamicsNEI] (IntelligentEnergistics-1.7.10-2.1.4.554-universal.jar) 
	UCHIJAAAA	inventorytweaks{1.59-dev-152-cf6e263} [Inventory Tweaks] (InventoryTweaks-1.59-dev-152.jar) 
	UCHIJAAAA	IronChest{6.0.62.742} [Iron Chest] (ironchest-1.7.10-6.0.62.742-universal.jar) 
	UCHIJAAAA	JABBA{1.2.1} [JABBA] (Jabba-1.2.1a_1.7.10.jar) 
	UCHIJAAAA	journeymap{@JMVERSION@} [JourneyMap] (journeymap-1.7.10-5.1.0-unlimited.jar) 
	UCHIJAAAA	mo{0.4.0-RC4} [Matter Overdrive] (MatterOverdrive-1.7.10-0.4.0-RC4.jar) 
	UCHIJAAAA	MineFactoryReloaded|CompatAppliedEnergistics{1.7.10R2.8.0} [MFR Compat: Applied Energistics] (MineFactoryReloaded-[1.7.10]2.8.0-104.jar) 
	UCHIJAAAA	MineFactoryReloaded|CompatAtum{1.7.10R2.8.0} [MFR Compat: Atum] (MineFactoryReloaded-[1.7.10]2.8.0-104.jar) 
	UCHIJAAAA	MineFactoryReloaded|CompatBackTools{1.7.10R2.8.0} [MFR Compat: BackTools] (MineFactoryReloaded-[1.7.10]2.8.0-104.jar) 
	UCHIJAAAA	MineFactoryReloaded|CompatBuildCraft{1.7.10R2.8.0} [MFR Compat: BuildCraft] (MineFactoryReloaded-[1.7.10]2.8.0-104.jar) 
	UCHIJAAAA	MineFactoryReloaded|CompatChococraft{1.7.10R2.8.0} [MFR Compat: Chococraft] (MineFactoryReloaded-[1.7.10]2.8.0-104.jar) 
	UCHIJAAAA	MineFactoryReloaded|CompatExtraBiomes{1.7.10R2.8.0} [MFR Compat: ExtraBiomes] (MineFactoryReloaded-[1.7.10]2.8.0-104.jar) 
	UCHIJAAAA	MineFactoryReloaded|CompatForestry{1.7.10R2.8.0} [MFR Compat: Forestry] (MineFactoryReloaded-[1.7.10]2.8.0-104.jar) 
	UCHIJAAAA	MineFactoryReloaded|CompatForgeMicroblock{1.7.10R2.8.0} [MFR Compat: ForgeMicroblock] (MineFactoryReloaded-[1.7.10]2.8.0-104.jar) 
	UCHIJAAAA	MineFactoryReloaded|CompatIC2{1.7.10R2.8.0} [MFR Compat: IC2] (MineFactoryReloaded-[1.7.10]2.8.0-104.jar) 
	UCHIJAAAA	Mystcraft{0.12.3.00} [Mystcraft] (mystcraft-1.7.10-0.12.3.00.jar) 
	UCHIJAAAA	MineFactoryReloaded|CompatMystcraft{1.7.10R2.8.0} [MFR Compat: Mystcraft] (MineFactoryReloaded-[1.7.10]2.8.0-104.jar) 
	UCHIJAAAA	MrTJPCoreMod{1.1.0.31} [MrTJPCore] (MrTJPCore-1.1.0.31-universal.jar) 
	UCHIJAAAA	ProjRed|Core{4.7.0pre9.92} [ProjectRed Core] (ProjectRed-1.7.10-4.7.0pre9.92-Base.jar) 
	UCHIJAAAA	ProjRed|Exploration{4.7.0pre9.92} [ProjectRed Exploration] (ProjectRed-1.7.10-4.7.0pre9.92-World.jar) 
	UCHIJAAAA	MineFactoryReloaded|CompatProjRed{1.7.10R2.8.0} [MFR Compat ProjectRed] (MineFactoryReloaded-[1.7.10]2.8.0-104.jar) 
	UCHIJAAAA	MineFactoryReloaded|CompatRailcraft{1.7.10R2.8.0} [MFR Compat: Railcraft] (MineFactoryReloaded-[1.7.10]2.8.0-104.jar) 
	UCHIJAAAA	MineFactoryReloaded|CompatSufficientBiomes{1.7.10R2.8.0} [MFR Compat: Sufficient Biomes] (MineFactoryReloaded-[1.7.10]2.8.0-104.jar) 
	UCHIJAAAA	MineFactoryReloaded|CompatThaumcraft{1.7.10R2.8.0} [MFR Compat: Thaumcraft] (MineFactoryReloaded-[1.7.10]2.8.0-104.jar) 
	UCHIJAAAA	MineFactoryReloaded|CompatThermalExpansion{1.7.10R2.8.0} [MFR Compat: Thermal Expansion] (MineFactoryReloaded-[1.7.10]2.8.0-104.jar) 
	UCHIJAAAA	MineFactoryReloaded|CompatTConstruct{1.7.10R2.8.0} [MFR Compat: Tinkers' Construct] (MineFactoryReloaded-[1.7.10]2.8.0-104.jar) 
	UCHIJAAAA	MineFactoryReloaded|CompatTwilightForest{1.7.10R2.8.0} [MFR Compat: TwilightForest] (MineFactoryReloaded-[1.7.10]2.8.0-104.jar) 
	UCHIJAAAA	MineFactoryReloaded|CompatVanilla{1.7.10R2.8.0} [MFR Compat: Vanilla] (MineFactoryReloaded-[1.7.10]2.8.0-104.jar) 
	UCHIJAAAA	MineTweaker3{3.0.10} [MineTweaker 3] (MineTweaker3-1.7.10-3.0.10.jar) 
	UCHIJAAAA	modtweaker2{0.9.2} [Mod Tweaker 2] (ModTweaker2-0.9.3.jar) 
	UCHIJAAAA	numina{0.4.0.119} [Numina] (Numina-0.4.0.119.jar) 
	UCHIJAAAA	powersuits{0.11.0.281} [MachineMuse's Modular Powersuits] (ModularPowersuits-0.11.0.281.jar) 
	UCHIJAAAA	Morph{0.9.2} [Morph] (Morph-Beta-0.9.2.jar) 
	UCHIJAAAA	Morpheus{1.7.10-1.6.10} [Morpheus] (Morpheus-1.7.10-1.6.10.jar) 
	UCHIJAAAA	NEIAddons{1.12.11.36} [NEI Addons] (neiaddons-1.12.11.36-mc1.7.10.jar) 
	UCHIJAAAA	NEIAddons|Developer{1.12.11.36} [NEI Addons: Developer Tools] (neiaddons-1.12.11.36-mc1.7.10.jar) 
	UCHIJAAAA	NEIAddons|AppEng{1.12.11.36} [NEI Addons: Applied Energistics 2] (neiaddons-1.12.11.36-mc1.7.10.jar) 
	UCHIJAAAA	NEIAddons|Botany{1.12.11.36} [NEI Addons: Botany] (neiaddons-1.12.11.36-mc1.7.10.jar) 
	UCHIJAAAA	NEIAddons|Forestry{1.12.11.36} [NEI Addons: Forestry] (neiaddons-1.12.11.36-mc1.7.10.jar) 
	UCHIJAAAA	NEIAddons|CraftingTables{1.12.11.36} [NEI Addons: Crafting Tables] (neiaddons-1.12.11.36-mc1.7.10.jar) 
	UCHIJAAAA	NEIAddons|ExNihilo{1.12.11.36} [NEI Addons: Ex Nihilo] (neiaddons-1.12.11.36-mc1.7.10.jar) 
	UCHIJAAAA	neiintegration{1.0.12} [NEI Integration] (NEIIntegration-MC1.7.10-1.0.12.jar) 
	UCHIJAAAA	notenoughkeys{@MOD_VERSION@} [NotEnoughKEys] (NotEnoughKeys-1.7.10-1.0.0b30.jar) 
	UCHIJAAAA	neresources{0.1.0.106} [Not Enough Resources] (NotEnoughResources-1.7.10-0.1.0.106.jar) 
	UCHIJAAAA	ObsidiPlates{3.0.0.18} [ObsidiPlates] (ObsidiPlates-1.7.10-universal-3.0.0.18.jar) 
	UCHIJAAAA	OpenMods{0.8} [OpenMods] (OpenModsLib-1.7.10-0.8.jar) 
	UCHIJAAAA	OpenBlocks{1.4.4} [OpenBlocks] (OpenBlocks-1.7.10-1.4.4.jar) 
	UCHIJAAAA	openmodularturrets{2.1.4-183} [Open Modular Turrets] (OpenModularTurrets-1.7.10-2.1.4-183.jar) 
	UCHIJAAAA	OpenPeripheralCore{1.2} [OpenPeripheralCore] (OpenPeripheralCore-1.7.10-1.2.jar) 
	UCHIJAAAA	OpenPeripheral{0.4} [OpenPeripheralAddons] (OpenPeripheralAddons-1.7.10-0.4.jar) 
	UCHIJAAAA	OpenPeripheralIntegration{0.3} [OpenPeripheralIntegration] (OpenPeripheralIntegration-1.7.10-0.3.jar) 
	UCHIJAAAA	MapWriter{2.1.2} [MapWriter] (Opis-1.2.5_1.7.10.jar) 
	UCHIJAAAA	Opis{1.2.5} [Opis] (Opis-1.2.5_1.7.10.jar) 
	UCHIJAAAA	PneumaticCraft{1.11.7-127} [PneumaticCraft] (PneumaticCraft-1.7.10-1.11.7-127-universal.jar) 
	UCHIJAAAA	PortalGun{4.0.0-beta-5} [PortalGun] (PortalGun-4.0.0-beta-5.jar) 
	UCHIJAAAA	ProjRed|Transmission{4.7.0pre9.92} [ProjectRed Transmission] (ProjectRed-1.7.10-4.7.0pre9.92-Integration.jar) 
	UCHIJAAAA	ProjRed|Compatibility{4.7.0pre9.92} [ProjectRed Compatibility] (ProjectRed-1.7.10-4.7.0pre9.92-Compat.jar) 
	UCHIJAAAA	ProjRed|Integration{4.7.0pre9.92} [ProjectRed Integration] (ProjectRed-1.7.10-4.7.0pre9.92-Integration.jar) 
	UCHIJAAAA	ProjRed|Illumination{4.7.0pre9.92} [ProjectRed Illumination] (ProjectRed-1.7.10-4.7.0pre9.92-Lighting.jar) 
	UCHIJAAAA	quiverchevsky{b100} [QuiverBow] (QuiverBow_1.7.10_b100.zip) 
	UCHIJAAAA	ResourceLoader{1.2} [Resource Loader] (ResourceLoader-1.2.jar) 
	UCHIJAAAA	rftools{3.35} [RFTools] (rftools-3.35.jar) 
	UCHIJAAAA	Roguelike{1.3.6.3} [Roguelike Dungeons] (roguelike-1.7.10-1.3.6.3.jar) 
	UCHIJAAAA	SolarExpansion{1.6a} [Solar Expansion] (SolarExpansion-Basic-1.6a.jar) 
	UCHIJAAAA	StevesFactoryManager{A93} [Steve's Factory Manager] (StevesFactoryManagerA93.jar) 
	UCHIJAAAA	StevesAddons{0.10.16} [Steve's Addons] (StevesAddons-1.7.10-0.10.16.jar) 
	UCHIJAAAA	StevesCarts{2.0.0.b18} [Steve's Carts 2] (StevesCarts2.0.0.b18.jar) 
	UCHIJAAAA	StevesWorkshop{0.5.1} [Steve's Workshop] (StevesWorkshop-0.5.1.jar) 
	UCHIJAAAA	TardisMod{0.99} [Tardis Mod] (tardismod-1.7.10-0.99-207.jar) 
	UCHIJAAAA	tcnodetracker{1.1.2} [TC Node Tracker] (tcnodetracker-1.7.10-1.1.2.jar) 
	UCHIJAAAA	thaumicenergistics{0.8.10.10} [Thaumic Energistics] (thaumicenergistics-0.8.10.10.jar) 
	UCHIJAAAA	ThermalDynamics{1.7.10R1.1.0} [Thermal Dynamics] (ThermalDynamics-[1.7.10]1.1.0-161.jar) 
	UCHIJAAAA	TiCTooltips{1.2.5} [TiC Tooltips] (TiCTooltips-mc1.7.10-1.2.5.jar) 
	UCHIJAAAA	TMechworks{0.2.14.100} [Tinkers' Mechworks] (TMechworks-1.7.10-0.2.14.100.jar) 
	UCHIJAAAA	Translocator{1.1.1.14} [Translocator] (Translocator-1.7.10-1.1.1.14-universal.jar) 
	UCHIJAAAA	WailaHarvestability{1.1.2} [Waila Harvestability] (WailaHarvestability-mc1.7.x-1.1.2.jar) 
	UCHIJAAAA	witchery{0.24.1} [Witchery] (witchery-1.7.10-0.24.1.jar) 
	UCHIJAAAA	WR-CBE|Core{1.4.1.9} [WR-CBE Core] (WR-CBE-1.7.10-1.4.1.9-universal.jar) 
	UCHIJAAAA	WR-CBE|Addons{1.4.1.9} [WR-CBE Addons] (WR-CBE-1.7.10-1.4.1.9-universal.jar) 
	UCHIJAAAA	WR-CBE|Logic{1.4.1.9} [WR-CBE Logic] (WR-CBE-1.7.10-1.4.1.9-universal.jar) 
	UCHIJAAAA	McMultipart{1.2.0.345} [Minecraft Multipart Plugin] (ForgeMultipart-1.7.10-1.2.0.345-universal.jar) 
	UCHIJAAAA	aobd{2.8.4} [Another One Bites The Dust] (AOBD-2.8.4.jar) 
	UCHIJAAAA	denseores{1.0} [Dense Ores] (denseores-1.6.2.jar) 
	UCHIJAAAA	VeganOption{0.1.2} [The Vegan Option] (VeganOption-mc1.7.10-0.1.2.jar) 
	UCHIJAAAA	ForgeMicroblock{1.2.0.345} [Forge Microblocks] (ForgeMultipart-1.7.10-1.2.0.345-universal.jar) 
	GL info: ' Vendor: 'NVIDIA Corporation' Version: '4.5.0 NVIDIA 346.96' Renderer: 'GeForce GTX 550 Ti/PCIe/SSE2'
	OpenModsLib class transformers: [stencil_patches:FINISHED],[movement_callback:FINISHED],[map_gen_fix:FINISHED],[gl_capabilities_hook:FINISHED],[player_render_hook:FINISHED]
	Class transformer null safety: all safe
	CoFHCore: -[1.7.10]3.0.3-303
	AE2 Version: stable rv2-stable-10 for Forge 10.13.2.1291
	Mantle Environment: Environment healthy.
	ThermalFoundation: -[1.7.10]1.2.0-102
	ThermalExpansion: -[1.7.10]4.0.3B1-218
	MineFactoryReloaded: -[1.7.10]2.8.0-104
	RedstoneArsenal: -[1.7.10]1.1.1-89
	TConstruct Environment: Environment healthy.
	ThermalDynamics: -[1.7.10]1.1.0-161
	List of loaded APIs: 
		* AncientWarfareAPI (1.0) from ancientwarfare-2.4.112-beta-MC1.7.10-FULL.jar
		* API_NEK (1.0.0) from NotEnoughKeys-1.7.10-1.0.0b30.jar
		* appliedenergistics2|API (rv2) from tardismod-1.7.10-0.99-207.jar
		* Baubles|API (1.0.1.10) from Baubles-1.7.10-1.0.1.10.jar
		* BiomesOPlentyAPI (1.0.0) from BiomesOPlenty-1.7.10-2.1.0.1067-universal.jar
		* BotaniaAPI (62) from Botania r1.7-221.jar
		* BuildCraftAPI|blocks (1.0) from buildcraft-7.0.21.jar
		* BuildCraftAPI|blueprints (1.3) from buildcraft-7.0.21.jar
		* BuildCraftAPI|boards (2.0) from buildcraft-7.0.21.jar
		* BuildCraftAPI|core (1.0) from buildcraft-7.0.21.jar
		* BuildCraftAPI|crops (1.1) from buildcraft-7.0.21.jar
		* BuildCraftAPI|events (1.0) from buildcraft-7.0.21.jar
		* BuildCraftAPI|facades (1.1) from buildcraft-7.0.21.jar
		* BuildCraftAPI|filler (4.0) from buildcraft-7.0.21.jar
		* BuildCraftAPI|fuels (2.0) from buildcraft-7.0.21.jar
		* BuildCraftAPI|gates (4.1) from buildcraft-7.0.21.jar
		* BuildCraftAPI|items (1.1) from buildcraft-7.0.21.jar
		* BuildCraftAPI|library (2.0) from buildcraft-7.0.21.jar
		* BuildCraftAPI|power (1.3) from buildcraft-7.0.21.jar
		* BuildCraftAPI|recipes (3.0) from buildcraft-7.0.21.jar
		* BuildCraftAPI|robotics (2.1) from buildcraft-7.0.21.jar
		* BuildCraftAPI|statements (1.0) from Railcraft_1.7.10-9.7.0.0.jar
		* BuildCraftAPI|tablet (1.0) from buildcraft-7.0.21.jar
		* BuildCraftAPI|tiles (1.2) from buildcraft-7.0.21.jar
		* BuildCraftAPI|tools (1.0) from extrautilities-1.2.11.jar
		* BuildCraftAPI|transport (4.0) from Railcraft_1.7.10-9.7.0.0.jar
		* CarpentersBlocks|API (3.3.7) from Carpenter's Blocks v3.3.7 - MC 1.7.10.jar
		* ChiselAPI (0.1.0) from Chisel2-2.5.1.44.jar
		* ChiselAPI|Carving (0.1.0) from Chisel2-2.5.1.44.jar
		* ChiselAPI|Rendering (0.1.0) from Chisel2-2.5.1.44.jar
		* CoFHAPI (1.7.10R1.0.1) from Railcraft_1.7.10-9.7.0.0.jar
		* CoFHAPI|block (1.7.10R1.0.13) from ProgressiveAutomation-1.7.10-1.6.27.jar
		* CoFHAPI|core (1.7.10R1.1.0) from EnderTech-1.7.10-0.3.2.399.jar
		* CoFHAPI|energy (1.7.10R1.0.0) from funky-locomotion-1.7.10-beta-7.jar
		* CoFHAPI|fluid (1.7.10R1.1.0) from EnderTech-1.7.10-0.3.2.399.jar
		* CoFHAPI|inventory (1.7.10R1.0.3) from SolarExpansion-Basic-1.6a.jar
		* CoFHAPI|item (1.7.10R1.0.13B2) from CoFHLib-[1.7.10]1.0.3-175.jar
		* CoFHAPI|items (1.7.10R1.0.3) from SolarExpansion-Basic-1.6a.jar
		* CoFHAPI|modhelpers (1.7.10R1.0.13B2) from CoFHLib-[1.7.10]1.0.3-175.jar
		* CoFHAPI|tileentity (1.7.10R1.0.3) from EnderIO-1.7.10-2.2.8.381.jar
		* CoFHAPI|transport (1.7.10R1.0.3) from SolarExpansion-Basic-1.6a.jar
		* CoFHAPI|world (1.7.10R1.0.3) from SolarExpansion-Basic-1.6a.jar
		* CoFHLib (1.7.10R1.0.3B3) from CoFHLib-[1.7.10]1.0.3-175.jar
		* CoFHLib|audio (1.7.10R1.0.3B3) from CoFHLib-[1.7.10]1.0.3-175.jar
		* CoFHLib|gui (1.7.10R1.0.3) from CoFHCore-[1.7.10]3.0.3-303.jar
		* CoFHLib|gui|container (1.7.10R1.0.3B3) from CoFHLib-[1.7.10]1.0.3-175.jar
		* CoFHLib|gui|element (1.7.10R1.0.4B1) from EnderTech-1.7.10-0.3.2.399.jar
		* CoFHLib|gui|element|listbox (1.7.10R1.0.3B3) from CoFHLib-[1.7.10]1.0.3-175.jar
		* CoFHLib|gui|slot (1.7.10R1.0.4B1) from EnderTech-1.7.10-0.3.2.399.jar
		* CoFHLib|inventory (1.7.10R1.0.3B3) from CoFHLib-[1.7.10]1.0.3-175.jar
		* CoFHLib|render (1.7.10R1.0.4B1) from EnderTech-1.7.10-0.3.2.399.jar
		* CoFHLib|render|particle (1.7.10R1.0.4B1) from EnderTech-1.7.10-0.3.2.399.jar
		* CoFHLib|util (1.7.10R1.0.3) from CoFHCore-[1.7.10]3.0.3-303.jar
		* CoFHLib|util|helpers (1.7.10R1.0.4B1) from EnderTech-1.7.10-0.3.2.399.jar
		* CoFHLib|util|position (1.7.10R1.0.3) from CoFHCore-[1.7.10]3.0.3-303.jar
		* CoFHLib|world (1.7.10R1.0.4B1) from EnderTech-1.7.10-0.3.2.399.jar
		* CoFHLib|world|feature (1.7.10R1.0.3) from CoFHCore-[1.7.10]3.0.3-303.jar
		* ComputerCraft|API (1.74) from ComputerCraft1.74.jar
		* ComputerCraft|API|FileSystem (1.74) from ComputerCraft1.74.jar
		* ComputerCraft|API|Lua (1.74) from ComputerCraft1.74.jar
		* ComputerCraft|API|Media (1.74) from ComputerCraft1.74.jar
		* ComputerCraft|API|Peripheral (1.74) from ComputerCraft1.74.jar
		* ComputerCraft|API|Permissions (1.74) from ComputerCraft1.74.jar
		* ComputerCraft|API|Redstone (1.74) from ComputerCraft1.74.jar
		* ComputerCraft|API|Turtle (1.74) from ComputerCraft1.74.jar
		* DraconicEvolution|API (1.1) from Draconic-Evolution-1.7.10-1.0.2-Snapshot_3.jar
		* EnderIOAPI (0.0.2) from EnderIO-1.7.10-2.2.8.381.jar
		* EnderIOAPI|Redstone (0.0.2) from EnderIO-1.7.10-2.2.8.381.jar
		* EnderIOAPI|Tools (0.0.2) from EnderIO-1.7.10-2.2.8.381.jar
		* ForestryAPI|apiculture (3.5.0) from forestry_1.7.10-3.6.6.24.jar
		* ForestryAPI|arboriculture (2.3.0) from forestry_1.7.10-3.6.6.24.jar
		* ForestryAPI|circuits (2.0.0) from forestry_1.7.10-3.6.6.24.jar
		* ForestryAPI|core (3.2.0) from forestry_1.7.10-3.6.6.24.jar
		* ForestryAPI|farming (1.1.0) from forestry_1.7.10-3.6.6.24.jar
		* ForestryAPI|food (1.1.0) from forestry_1.7.10-3.6.6.24.jar
		* ForestryAPI|fuels (2.0.1) from forestry_1.7.10-3.6.6.24.jar
		* ForestryAPI|genetics (3.3.0) from forestry_1.7.10-3.6.6.24.jar
		* ForestryAPI|hives (4.1.0) from forestry_1.7.10-3.6.6.24.jar
		* ForestryAPI|lepidopterology (1.1) from forestry_1.7.10-3.6.6.24.jar
		* ForestryAPI|mail (3.0.0) from forestry_1.7.10-3.6.6.24.jar
		* ForestryAPI|recipes (3.1.0) from forestry_1.7.10-3.6.6.24.jar
		* ForestryAPI|storage (3.0.0) from forestry_1.7.10-3.6.6.24.jar
		* ForestryAPI|world (1.1.0) from forestry_1.7.10-3.6.6.24.jar
		* Galacticraft API (1.0) from GalacticraftCore-1.7-3.0.12.357.jar
		* GameAnalyticsAPI (1.1) from AuraCascade-552.jar
		* GardenCoreAPI (1.0.0) from GardenStuff-1.7.10-1.6.8.jar
		* gendustryAPI (2.2.0) from gendustry-1.6.0.125-mc1.7.10.jar
		* Guide-API|API (1.7.10-1.0.1-20) from Guide-API-1.7.10-1.0.1-20.jar
		* IC2API (1.0) from Railcraft_1.7.10-9.7.0.0.jar
		* ImmersiveEngineering|API (1.0) from ImmersiveEngineering-0.5.4.jar
		* inpure|api (1.7) from INpureCore-[1.7.10]1.0.0B9-62.jar
		* MatterOverdrive|API (0.4.0-RC3) from MatterOverdrive-1.7.10-0.4.0-RC4.jar
		* McJtyLib (1.5.0) from mcjtylib-1.5.0.jar
		* Mystcraft|API (0.1) from mystcraft-1.7.10-0.12.3.00.jar
		* neresources|API (1.0) from NotEnoughResources-1.7.10-0.1.0.106.jar
		* OpenBlocks|API (1.0) from OpenBlocks-1.7.10-1.4.4.jar
		* OpenPeripheralApi (3.3.1) from OpenPeripheralCore-1.7.10-1.2.jar
		* PneumaticCraftApi (1.0) from PneumaticCraft-1.7.10-1.11.7-127-universal.jar
		* RailcraftAPI|bore (1.0.0) from Railcraft_1.7.10-9.7.0.0.jar
		* RailcraftAPI|carts (1.5.0) from Railcraft_1.7.10-9.7.0.0.jar
		* RailcraftAPI|core (1.4.0) from Railcraft_1.7.10-9.7.0.0.jar
		* RailcraftAPI|crafting (1.0.0) from ImmersiveEngineering-0.5.4.jar
		* RailcraftAPI|electricity (1.6.0) from Railcraft_1.7.10-9.7.0.0.jar
		* RailcraftAPI|events (1.0.0) from Railcraft_1.7.10-9.7.0.0.jar
		* RailcraftAPI|fuel (1.0.0) from Railcraft_1.7.10-9.7.0.0.jar
		* RailcraftAPI|helpers (1.1.0) from Railcraft_1.7.10-9.7.0.0.jar
		* RailcraftAPI|items (1.0.0) from Railcraft_1.7.10-9.7.0.0.jar
		* RailcraftAPI|locomotive (1.0.1) from Railcraft_1.7.10-9.7.0.0.jar
		* RailcraftAPI|signals (1.3.4) from Railcraft_1.7.10-9.7.0.0.jar
		* RailcraftAPI|tracks (2.0.0) from Railcraft_1.7.10-9.7.0.0.jar
		* Thaumcraft|API (4.2.2.0) from tardismod-1.7.10-0.99-207.jar
		* WailaAPI (1.2) from Waila-1.5.10_1.7.10.jar
	Stencil buffer state: Function set: GL30, pool: forge, bits: 8
	AE2 Integration: IC2:OFF, RotaryCraft:OFF, RC:ON, BC:ON, RF:ON, RFItem:ON, MFR:ON, DSU:ON, FZ:OFF, FMP:ON, RB:OFF, CLApi:OFF, Waila:ON, InvTweaks:ON, NEI:ON, CraftGuide:OFF, Mekanism:OFF, ImmibisMicroblocks:OFF, BetterStorage:OFF
	Launched Version: 1.7.10-Forge10.13.4.1448-1.7.10
	LWJGL: 2.9.1
	OpenGL: GeForce GTX 550 Ti/PCIe/SSE2 GL version 4.5.0 NVIDIA 346.96, NVIDIA Corporation
	GL Caps: Using GL 1.3 multitexturing.
Using framebuffer objects because OpenGL 3.0 is supported and separate blending is supported.
Anisotropic filtering is supported and maximum anisotropy is 16.
Shaders are available because OpenGL 2.1 is supported.

	Is Modded: Definitely; Client brand changed to 'fml,forge'
	Type: Client (map_client.txt)
	Resource Packs: []
	Current Language: Español (España)
	Profiler Position: N/A (disabled)
	Vec3 Pool Size: 0 (0 bytes; 0 MB) allocated, 0 (0 bytes; 0 MB) used
	Anisotropic Filtering: Off (1)